/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication21;

import java.util.Scanner;

/**
 *
 * @author alvin
 */
public class Ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	Scanner reader = new Scanner(System.in);
        
        //pedimos el numero
	System.out.println("Ingrese la fecha: ");
	String number = reader.nextLine();
       
        //obtenemos las dos fechas
        int[] finalDates = getDates(number);
        //obtenemos el espacio que se requiere
        getDataSpace(finalDates[0], finalDates[1]);
        
	
   }
    
   static void getDataSpace(int lim1, int lim2){
       int spaceNum = 0;
       for(int x = lim1; x<= lim2; x++){
           if(getRomanNumber(x).length() > spaceNum){
               spaceNum = getRomanNumber(x).length();
           }   
       }
       
       System.out.println("Espacio requerido = " + spaceNum);
   }
 
   static String getRomanNumber(int num) {
	// Variaciones de numeros romanos
	String[] milesArray = {"", "M", "MM"};
	String[] centenasArray = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
	String[] decenasArray = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
	String[] unidadesArray = {"", "I", "II", "III", "IV", "V", "VI","VII", "VIII", "IX", "X" };
        
	int miles = num / 1000;
	int centenas = (num - (miles * 1000)) / 100;
	int decenas = (num - ((miles * 1000) + (centenas * 100))) / 10;
	int unidades = num - ((miles * 1000) + (centenas * 100) + (decenas * 10));

        String numRomano = milesArray[miles] + centenasArray[centenas] + decenasArray[decenas] + unidadesArray[unidades];
        return numRomano;
   }
   
   static int[] getDates(String num){
       String[] arrOfStr = num.split("-", 2);
       String[] dateOne = arrOfStr[0].split("A|B", 2);
       String[] dateTwo = arrOfStr[1].split("A|B", 2);
       int finalDateOne = 0;
       int finalDateTwo = 0;
       int finalDates[];
       finalDates = new int[2];
       
       //Validamos que no se ingresen datos que no estan permitos como 1AD - 1BC o 1BC-2BC 
       if(dateOne[1].equals("D") && dateTwo[1].equals("C")){
           System.out.println("formato no valido");
           System.exit(0);
       }else if(dateTwo[1].equals("C") && dateOne[1].equals("C")){
           if(Integer.parseInt(dateOne[0]) < Integer.parseInt(dateTwo[0])){
               System.out.println("formato no valido");
               System.exit(0);
           }
       }
       
       
       if(dateTwo[1].equals("D")){
           finalDateTwo = Integer.parseInt(dateTwo[0]) + 753;
       }else if(dateTwo[1].equals("C")){
           finalDateTwo = 753 - Integer.parseInt(dateTwo[0]) + 1;
       }
       
       if(dateOne[1].equals("D")){
           finalDateOne = Integer.parseInt(dateOne[0]) + 753;
       }else if(dateOne[1].equals("C")){
           finalDateOne = 753 - Integer.parseInt(dateOne[0]) + 1;
       }
       
       
       System.out.println("d1 = " + finalDateOne);
       System.out.println("d2 = " + finalDateTwo);
       
       
       finalDates[0] = finalDateOne;
       finalDates[1] = finalDateTwo;
       
       return finalDates;
       
   }
    
}
